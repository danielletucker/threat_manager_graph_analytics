# Updated 5 20 2021
# Constructs graphs from all historical data
# Collects degree for all users through all historical data for a new client.

# Consult Rakesh Kumar or Rafiq Mohammadi or Dani Tucker

import sys
import datetime 
import glob
import statistics as stat
import pickle
from datetime import date, timedelta
from multiprocessing import Pool
import networkx as nx
from functools import partial
import math 
import calendar

# Get script dependencies
import main_module

EDGE_THRESHOLD = 1
GRAPH_AGE = 14
ITEMS = ["doc", "matter", "client"]
CORES = 2

def load_data(dat_file, txt_file):
    """
    written 5 20 2021
    load data for graphs
    :param dat_file: company data
    :param txt_file: dates for training
    :return: user object, start_date and end_date
    """
    dates = [line.rstrip('\n') for line in txt_file]
    start_date = dates[0]
    end_date = dates[1]
    user_object = main_module.RiskAnalysis()
    return(user_object, start_date, end_date)

def initialize(user_object, start_date, end_date, dat_file, item):
    """
    written 5 20 2021
    transform data for graph building
    :param user_object
    :param start_date
    :param end_date
    :param dat_file: filename with company data
    :param item: item (1,2,3) specifying doc, matter, or client
    :return: user_par_dictionary: <date>:<user:connections>
    """

    print("Building " + ITEMS[(item-1)] + " Networks")
    user_pair_dict = user_object.initialize_company_network(pickle_file=dat_file, 
                        start_date=start_date, 
                        end_date=end_date, 
                        edge_threshold= EDGE_THRESHOLD,
                        network_type=item)
    return(user_pair_dict)

def construct_graphs(user_object, user_pair_dict, start_date, end_date, max_date):
    """
    written 5 20 2021
    construct graph across training dates
    :user_object
    :user_pair_dict
    :start_date
    :end_date
    :param max_date: end of GRAPH_AGE date window
    :return degree_list: [max_date, degrees]
    Note: the degrees are in order of historical users.....update graph will be difficult because of this.
    """    

    min_date = max_date - timedelta(days=GRAPH_AGE)
    current_pair_dict = {}
    for pair in user_pair_dict.keys():
        temp = {date(k[0],k[1],k[2]):v for k,v in user_pair_dict[pair].items()}
        current_pair_dict[pair] = {k:v for k,v in temp.items() if  min_date <= k <=max_date}
    connections = {}
    connections2 = {}
    for pairs in current_pair_dict.keys():
        connections[pairs] = {k:v for k,v in current_pair_dict[pairs].items() if v!= set()}
    connections2 = {k:v for k,v in connections.items() if v!={}}
    network_dict = {}

    for user in user_object.user_dict.keys():
        temp = {k[1]:v for k,v in connections2.items() if k[0] == user}
        network_dict[user] = [[k, len(v)] for k,v in temp.items()]
    G = nx.Graph()

    # create all the nodes
    G.add_nodes_from(list(user_object.user_dict.keys()))
    for network_user in list(user_object.user_dict.keys()):      
        connection_list = network_dict[network_user]
        for edge_info in connection_list:
            to_user = edge_info[0]
            edge_strength = edge_info[1]
            if edge_strength > 1:
                G.add_edges_from([(network_user, to_user, {'weight': edge_strength})])
    graph_list = [max_date,dict(G.degree())]
    return(graph_list)


def get_all_dates(start_date, end_date):
    """
    get all dates in training data
    :param start_date
    :param end_date
    :return list of dates
    """
    sdt = datetime.datetime.strptime(start_date, '%m/%d/%Y')
    edt = datetime.datetime.strptime(end_date, '%m/%d/%Y')

    sdate = date(sdt.year, sdt.month, sdt.day) + timedelta(days=GRAPH_AGE)   # start date
    edate = date(edt.year, edt.month, edt.day)  # end date

    delta = edate - sdate       # as timedelta
    all_dates ={}
    for i in range(delta.days + 1):
        all_dates[i] = sdate + timedelta(days=i)

    dates = list(all_dates.values())
    return(dates)

def convert_dates_in_degree_dict(degree_dict, dates):
    """
    put dates in formate cohesive with other scripts
    """
    degree_dict2 = {}
    for dat in dates:
        temp = math.floor(datetime.datetime.utcfromtimestamp(calendar.timegm(dat.timetuple())).timestamp()/60/24/60)
        degree_dict2[temp] = degree_dict[dat]
    return(degree_dict2)

def save_file(degree_dicts):
    f = open(str("/new_data/degree_dicts.pkl"),"wb")
    pickle.dump(degree_dicts,f)
    f.close()


if __name__ == '__main__':
    # data file must be in same folder as main and end in ".dat"
    dat_file1 = "/new_data/custa_cut_data.dat"
    txt_file1 = open("/new_data/dates.txt","r")
    user_object1, start_date1, end_date1 = load_data(dat_file1, txt_file1)
    dates1 = get_all_dates(start_date1, end_date1)
    degree_dicts = {}
    for item in range(1, (len(ITEMS)+1),1):
        user_pair_dict1 = initialize(user_object1, start_date1, end_date1, dat_file1, item)
        func = partial(construct_graphs, user_object1, user_pair_dict1, start_date1, end_date1)
        p = Pool(CORES)
        degree_dict1 = p.map(func, dates1)
        degree_dict2 = {i[0]:i[1] for i in degree_dict1}
        string = ITEMS[(item-1)] + "_degree"
        degree_dicts[string] = convert_dates_in_degree_dict(degree_dict2, dates1)
    save_file(degree_dicts)