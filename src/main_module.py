# module created on 5 6 2020
# updated on 5 19 2021
# implements anomaly detection 4 ways
# Z score method, single Poisson Posterior, Mixed Poisson Posterior, Non-Parametric Smoother
# Also had functions to create hub-spoke graphs
# As well as functions to create company wide graphs

import pickle
import time
import logging
from datetime import datetime
import pandas as pd
from scipy.stats import zscore
import networkx as nx
import statistics as stat
import scipy as sp

year_filter_key = 'Year'
month_filter_key = 'Month'
day_filter_key = 'Day'

DOC_ID = 1
MATTER_ID = 2
CLIENT_ID = 3

class RiskAnalysis:

    def __init__(self):
        """
        class initializer
        """
        self.logger = logging.getLogger('Doc Analysis')
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(message)s')

        self.dict_initialized = False  # indicates if time series data has been read from the disk

        """
        <user id>:<doc dict>
        <doc_id>:[[activity_code, epoch time stamp, doc version].....]
        """
        self.user_dict = {}            # user time series data

        self.cache = False             # cached results enabled or disabled
        self.risk_cache = {}           # cached anomaly results
        self.activity_cache = {}
        self.doc_cache = {}
        self.code_mapping_dict = {}    # maps activity code to label string
        self.unique_users_list = []    # unique users in the data set
        self.doc_to_matter = {}        # <doc id>:<matter id>
        self.matter_to_client = {}     # <matter id>:<client id>
        self.matter_to_doc = {}        # <matter id>:[doc_id, doc_id, .....]
        self.client_to_matter = {}     # <client id>:[matter_id, matter_id, ......]

    def cache_enabled(self):
        """
        verified 5 7 2019
        caching enabled"""
        self.cache = True

    def cache_disabled(self):
        """
        caching disabled
        """
        self.cache = False
        self.risk_cache = {}
        self.activity_cache = {}
        self.doc_cache = {}

    def display_info_message(self, msg_str):
        """
        verified 5 6 2020
        displays an information
        :param msg_str:
        :return:
        """
        self.logger.info(msg_str)

    def _get_time_struct(self, date_str):
        """
        verified 5 6 2020
        get time struct from ascii date string
        date is in zero padded month/day/year format year includes century
        :param date_str:
        :return:
        """
        format_str = '%m/%d/%Y'
        return time.strptime(date_str, format_str)

    def read_pickle_file(self, pickle_name):
        """
        verified 5 6 2020
        read the python data structures from disk
        :param pickle_name: pickled file containing the dictionary
        :return:
        """
        """
            the following structures are built
            self.code_mapping_dict
                <activity code>:activity label string
            self.user_dict
                <user id>:<doc dict>
                doc_dict
                    <doc_id>:[[activity_code, epoch time stamp, doc version].....]
            min_date minimum date in the data set '%Y-%m-%d %H:%M:%S'
            max_date maximum date in the data set '%Y-%m-%d %H:%M:%S'
            self.doc_to_matter
                <doc id>:matter_id
            self.matter_to_client
                <matter id>:client id
            self.matter_to_doc
                <matter id>:[doc_id, doc_id, ....]
            self.client_to_matter
                <client id>:[matter_id, matter_id .....]
        """

        self.logger.info('{} Reading Serialized Object'.format(time.asctime()))
        py_list = pickle.load(open(pickle_name, 'rb'))
        self.code_mapping_dict = py_list[0]
        self.user_dict = py_list[1]
        min_date = py_list[2][0]
        max_date = py_list[2][1]
        self.doc_to_matter = py_list[3]
        self.matter_to_client = py_list[4]
        self.matter_to_doc = py_list[5]
        self.client_to_matter = py_list[6]
        self.dict_initialized = True
        self.logger.info('{} Objects re-constructed'.format(time.asctime()))
        min_str = time.strftime('%Y-%m-%d %H:%M:%S', min_date)
        max_str = time.strftime('%Y-%m-%d %H:%M:%S', max_date)
        self.logger.info('Start time(UTC) {} End time(UTC) {}'.format(min_str, max_str))
        self.logger.info('Number of unique users {:,}'.format(len(self.user_dict)))
        self.logger.info('Number of unique documents {:,}'.format(len(self.doc_to_matter)))
        self.logger.info('Number of unique matters {:,}'.format(len(self.matter_to_doc)))
        self.logger.info('Number of unique clients {:,}'.format(len(self.client_to_matter)))
        return min_date, max_date
       
    def stdev2(self, count_list):
        """
        created 7 10 2020
        standard deviation function only one count
        :param list of counts
        :return: standard deviation of list
        """
        if len(count_list)<2:
            sd=1
        else:
            sd=stat.stdev(count_list)
        return sd

    def is_epoch_inrange(self, start_struct, end_struct, epoch_time):

        """
        verified 5 6 2020
        check to see if the epoch time is within the start and end date
        :param start_struct: start date time structure
        :param end_struct: end date time structure
        :param epoch_time: activity time stamp
        :return:
        """

        utc_struct = time.gmtime(epoch_time)

        start_epoch = time.mktime(start_struct)
        end_epoch = time.mktime(end_struct)

        if epoch_time >= start_epoch and epoch_time <= end_epoch:
            return True, utc_struct
        else:
            return False, utc_struct

    def fast_interval_check(self, start_epoch, end_epoch, epoch_time):

        """
        created 5 7 2020
        check to see if the epoch time is within the start and end date
        :param start_epoch: start epoch time
        :param end_epoch: end epoch time structure
        :param epoch_time: activity time stamp
        :return:
        """
        if epoch_time >= start_epoch and epoch_time <= end_epoch:
            utc_struct = time.gmtime(epoch_time)
            return True, utc_struct
        else:
            return False, None

    def get_epoch_day(self, epochtime):
        """
        verified 5 6 2020
        resolves to the epoch day i.e. the distinction within the day is lost
        :param epochtime:
        :return: epoch day
        """
        return epochtime // (24 * 60 * 60)

    def increment_count(self, dictn, uid, epoch_day_key):
        """
        verified 5 6 2020
        increment activity count
        :param dict: <uid>:day_dict where day_dict is <epoch day>:<count>
        :param uid: user id
        :param epoch_day_key: epoch day
        :return:updates the dict by incrementing activity count
            if user not in dict dict[uid]={<epoch_day>:1} allocate a new day_dict and insert day count
            else if epoch day not found for user insert <epoch_day>:1 in existing day_dict for user=uid
            else get set from dict[uid][epoch day] += 1
        """
        if uid not in dictn:
            dictn[uid] = {epoch_day_key: 1}
        else:
            day_dict = dictn[uid]
            if epoch_day_key not in day_dict:
                day_dict[epoch_day_key] = 1
            else:
                day_dict[epoch_day_key] += 1

    def insert_id(self, dictn, uid, epoch_day_key, doc_id):

        """
        verified 5 6 2020
        add id
        :param dict: <uid>:<epoch day>:<document id set>
        :param epoch_day_key: epoch day
        :param doc_id:
        :return: updates the dict by adding the doc_id
            if user not in dict dict[uid]={<epoch_day>:set(doc_id)} allocate a new day_dict and insert
            else if epoch day not found for user insert <epoch_day>:set(doc_id)
                 in existing day_dict for user=uid
            else get set from dict[uid][epoch day] and add doc_id to it
        """
        if uid not in dictn:
            doc_set = set()
            doc_set.add(doc_id)
            dictn[uid] = {epoch_day_key: doc_set}
        else:
            day_dict = dictn[uid]
            if epoch_day_key not in day_dict:
                doc_set = set()
                doc_set.add(doc_id)
                day_dict[epoch_day_key] = doc_set
            else:
                doc_set = day_dict[epoch_day_key]
                doc_set.add(doc_id)

    def convert_to_counts(self, dictn):
        """
        verified 5 6 2020
        :param dict: <uid>:<epoch day>:set(id1, id2, ...id n) where id is doc, matter or client id
        :return: <uid>:<epoch day>:len(set)
        self modifying code :(-
        """
        for uid in dictn:
            day_dict = dictn[uid]
            for epoch_day in day_dict:
                doc_set = day_dict[epoch_day]
                day_dict[epoch_day] = len(doc_set)

    def get_time_series(self, dictn):
        """
        verified 5 6 2020
        helper function transforms the passed dict as described below
        :param dict: <uid>:<epoch_day>:<count>
        :return: <uid>:[count1, count2 ... countn]
        flattens it to an array of numbers for time series analysis
        """
        new_dict = {}
        for uid in dictn:
            day_dict = dictn[uid]
            count_list = []
            for epoch_day in day_dict:
                count_list.append(day_dict[epoch_day])
            new_dict[uid] = count_list
        return new_dict

    def all_users_series(self, pickle_file, start_date, end_date, verbose=True):
        """
        verified 5 6 2020
        build time series and by day information
        this is a work horse function but the code is tight and relatively well contained
        make changes carefully here
        :param pickle_file:
        :param start_date:
        :param end_date:
        :param verbose:
        :return:
                    we are going to return the following information
                    <uid>:[activity_count, activity_count, ........]                time series
                    <uid>:[unique_doc_count, activity_count, ........]              time series
                    <uid>:[unique_matter_count, unique_matter_count, ........]      time series
                    <uid>:[unique_client_count, unique_client_count, ........]      time series
                    <uid>:<epoch day>:<activity count>
                    <uid>:<epoch day>:<unique doc accessed count>
                    <uid>:<epoch day>:<unique matter accessed count>
                    <uid>:<epoch day>:<unique client accessed count>
        """
        if verbose:
            self.logger.info('Building time series for all users')

        # validate the dates passed
        try:
            start_struct = self._get_time_struct(date_str=start_date)
            end_struct = self._get_time_struct(date_str=end_date)
        except ValueError as e:
            self.logger.error("Date conversion error {}".format(e))
            return

        if not self.dict_initialized:
            self.read_pickle_file(pickle_name=pickle_file)

        start_epoch = time.mktime(start_struct)
        end_epoch = time.mktime(end_struct)

        """post pickle file read we have the following dictionary
            <user id>:<doc dict>
            doc_dict
                <doc_id>:[ [activity_code, epoch time stamp, doc version].....]
        """

        # build the following dictionaries
        """
            all_act_dict number of user activities by day (across all activity types)
            all_doc_dict document ids accessed daily by a user
            all_matter_dict matter ids accessed daily by a user
            all_client_dict client ids accessed daily by a user
        """
        all_act_dict = {}        # <uid>:<epoch day>:<activity count>
        all_doc_dict = {}        # <uid>:<epoch day>:<document set>
        all_matter_dict = {}     # <uid>:<epoch day>:<matter set>
        all_client_dict = {}     # <uid>:<epoch day>:<client set>

        for uid in self.user_dict:
            id_dict = self.user_dict[uid]
            for doc_id in id_dict.keys():
                activity_list = id_dict[doc_id]
                for entry_info in activity_list:
                    status, utc_struct = self.fast_interval_check(start_epoch=start_epoch,
                                                                  end_epoch=end_epoch,
                                                                  epoch_time=entry_info[1])
                    # if not in range skip
                    if not status:
                        # self.logger.info('Skipping start {} end {} found {}'
                        #                  .format(start_date, end_date, utc_struct))
                        continue
                    # we will insert by epoch day
                    insert_key = self.get_epoch_day(entry_info[1])
                    self.increment_count(dictn=all_act_dict,
                                         uid=uid,
                                         epoch_day_key=insert_key)
                    self.insert_id(dictn=all_doc_dict,
                                   uid=uid,
                                   epoch_day_key=insert_key,
                                   doc_id=doc_id)

        # build the by user by day unique matter and client sets
        for uid in all_doc_dict:
            day_dict = all_doc_dict[uid]
            day_matter_dict = {}
            day_client_dict = {}
            for epoch_day in day_dict:
                doc_set = day_dict[epoch_day]
                matter_set = set()
                client_set = set()
                for docid in doc_set:
                    if docid in self.doc_to_matter:
                        matter_id = self.doc_to_matter[docid]
                        matter_set.add(matter_id)
                        if matter_id in self.matter_to_client:
                            client_id = self.matter_to_client[matter_id]
                            client_set.add(client_id)
                # finished processing and epoch day
                day_matter_dict[epoch_day] = matter_set
                day_client_dict[epoch_day] = client_set
            # finished processing all the epoch days for a specific user
            all_matter_dict[uid] = day_matter_dict
            all_client_dict[uid] = day_client_dict

        # transform the doc dict to counts self modifying code
        # <uid>:<epoch day>:<count>
        # where the count is
        # unique documents by day in all_doc_dict
        # unique matters by day in all_doc_dict
        # unique clients by day in all_doc_dict

        self.convert_to_counts(all_doc_dict)
        self.convert_to_counts(all_matter_dict)
        self.convert_to_counts(all_client_dict)

        if verbose:
            self.logger.info('Time series build completed')

        """
            we are going to return the following information
            <uid>:[activity_count, activity_count, ........]                time series
            <uid>:[unique_doc_count, activity_count, ........]              time series
            <uid>:[unique_matter_count, unique_matter_count, ........]      time series
            <uid>:[unique_client_count, unique_client_count, ........]      time series
            
            # <uid>:<epoch day>:<activity count>
            # <uid>:<epoch day>:<unique doc accessed count>
            # <uid>:<epoch day>:<unique matter accessed count>
            # <uid>:<epoch day>:<unique client accessed count>
            
        """
        ret_tuple = (
                        self.get_time_series(all_act_dict),
                        self.get_time_series(all_doc_dict),
                        self.get_time_series(all_matter_dict),
                        self.get_time_series(all_client_dict),
                        all_act_dict,
                        all_doc_dict,
                        all_matter_dict,
                        all_client_dict,
                    )

        return ret_tuple

    def get_training_data(self,
                          pickle_file,
                          training_start,
                          training_end
                          ):
        """
        verified 5 6 2020
        read training data from pickled file
        :param pickle_file: data file name
        :param training_start: '<month/date/year (4 digits)
        :param training_end: '<month/date/year (4 digits)
        :return:
        """
        # build the training data
        return self.all_users_series(pickle_file=pickle_file,
                                     start_date=training_start,
                                     end_date=training_end,
                                     verbose=True)
    
    

    def make_key_from_interval(self, interval_dict):
        """
        make a cache key from the interval dict
        :param interval_dict:
                        {
                            year_filter_key: (start_year, end_year),
                            month_filter_key: (start_month, end_month),
                            day_filter_key: (start_day, end_day)
                        }
        :return:
        """
        def tuple_to_string(dict_key):
            ret_string = str(interval_dict[dict_key][0])
            ret_string += str(interval_dict[dict_key][1])
            return ret_string

        if 'Year' in interval_dict and 'Month' in interval_dict and 'Day' in interval_dict:
            compound_key = tuple_to_string(dict_key='Year')
            compound_key += tuple_to_string(dict_key='Month')
            compound_key += tuple_to_string(dict_key='Day')
            return compound_key
        else:
            self.logger.error("Error: Compound key error {}".format(interval_dict))
            return None

    def _check_time_parameters(self, epoch_time, interval_dict):

        """
        checks to see if the parameters are in range
        :param epoch_time: time in seconds
        :param interval_dict:
                        {
                            year_filter_key: (start_year, end_year),
                            month_filter_key: (start_month, end_month),
                            day_filter_key: (start_day, end_day)
                        }
        :return:
        """
        utc_struct = time.gmtime(epoch_time)
        time_dict = {year_filter_key: utc_struct[0],
                     month_filter_key: utc_struct[1],
                     day_filter_key: utc_struct[2]
                     }

        parameter_order_list = [
                                year_filter_key,
                                month_filter_key,
                                day_filter_key,
                               ]

        for validation_key in parameter_order_list:
            if validation_key not in time_dict or validation_key not in interval_dict:
                self.logger.error('Key {} not found in _check_time_parameters'.format(validation_key))
                exit(1)
            # get the time entity that needs to be validated - year or month or day
            time_entity = time_dict[validation_key]
            # if the range tuple is empty let all values through
            range_tuple = interval_dict[validation_key]
            if len(range_tuple) == 0:
                continue
            lo = range_tuple[0]
            hi = range_tuple[1]
            if time_entity < lo or time_entity > hi:
                return False
        return True

    def activity_user_df(self, pickle_file, interval_dict):
        """
        builds a panda data frame with user activity
        :param pickle_file: file name containing the python user activity data structures
        :param interval_dict:
                        {
                            year_filter_key: (start_year, end_year),
                            month_filter_key: (start_month, end_month),
                            day_filter_key: (start_day, end_day)
                        }
        :return: panda data frame see below
        """
        compound_key = self.make_key_from_interval(interval_dict)
        if self.cache:
            if compound_key is not None and compound_key in self.activity_cache:
                self.logger.info("Info: Cache hit {}".format(compound_key))
                return self.activity_cache[compound_key]

        self.logger.info('User Analysis Started')
        if not self.dict_initialized:
            self.read_pickle_file(pickle_name=pickle_file)

        """post pickle file read we have the following dictionary
            <user id>:<doc dict>
            doc_dict
                <doc_id>:[ [activity_code, epoch time stamp, doc version].........]
        """

        panda_list = []
        user_list = []
        z_list = []
        for uid in self.user_dict.keys():
            id_dict = self.user_dict[uid]
            total_count = 0.0
            for docid in id_dict.keys():
                activity_list = id_dict[docid]
                for item_list in activity_list:
                    epoch_time = item_list[1]
                    if self._check_time_parameters(epoch_time=epoch_time,
                                                   interval_dict=interval_dict):
                        total_count += 1
            user_list.append(uid)
            panda_list.append({'Activity Count': round(total_count, 2)})
            z_list.append(total_count)

        # return the panda data frame
        z_list = zscore(z_list).tolist()
        for index, dict_item in enumerate(panda_list):
            dict_item['Zscore'] = round(z_list[index], 2)
        self.logger.info('User Analysis Completed')
        df = pd.DataFrame(panda_list, index=user_list).sort_values(by=['Activity Count'], ascending=[0])
        if self.cache and compound_key is not None:
            self.activity_cache[compound_key] = df
            self.logger.info("Info: Activity Cache updated {}".format(compound_key))
        return df

    def get_content_metrics(self, pickle_file, start_date, end_date):
        """
        get a panda data fram that contains unique document, matter and client activities
        :param pickle_file:
        :param start_date:
        :param end_date:
        :return:
        """
        # get the time series data needed for the analysis
        """
                    we are going to get the following information
                    <uid>:[activity_count, activity_count, ........]                time series
                    <uid>:[unique_doc_count, activity_count, ........]              time series
                    <uid>:[unique_matter_count, unique_matter_count, ........]      time series
                    <uid>:[unique_client_count, unique_client_count, ........]      time series
                    <uid>:<epoch day>:<activity count>
                    <uid>:<epoch day>:<unique doc accessed count>
                    <uid>:<epoch day>:<unique matter accessed count>
                    <uid>:<epoch day>:<unique client accessed count>
        """
        data_tuple = self.all_users_series(pickle_file=pickle_file,
                                           start_date=start_date,
                                           end_date=end_date,
                                           verbose=True)
        # times series by user for unique doc matter and client activity within the dates specified
        doc_dict = data_tuple[1]
        matter_dict = data_tuple[2]
        client_dict = data_tuple[3]

        # build counts for the time period
        def local_sum_dict(data_dict):
            """
            sums a time series dict
            :param data_dict: <uid>:[count1, count2 ....]
            :return: <uid>:count
            """
            new_dict = {}
            for uid in data_dict:
                new_dict[uid] = sum(data_dict[uid])
            return new_dict

        count_doc_dict = local_sum_dict(doc_dict)
        count_matter_dict = local_sum_dict(matter_dict)
        count_client_dict = local_sum_dict(client_dict)

        def extract_count(data_dict, a_uid):
            """
            extracts the count associated with the user
            :param data_dict: <uid>:<count>
            :param a_uid:
            :return: <count> or zero
            """
            if a_uid in data_dict:
                return data_dict[uid]
            else:
                return 0

        # build a list of users - we can use any one of the three dicts
        panda_dict_list = []
        user_list = count_doc_dict.keys()
        for uid in user_list:
            doc_count = extract_count(data_dict=count_doc_dict, a_uid=uid)
            matter_count = extract_count(data_dict=count_matter_dict, a_uid=uid)
            client_count = extract_count(data_dict=count_client_dict, a_uid=uid)
            panda_dict_list.append(
                                    {
                                        'Document': doc_count,
                                        'Matter': matter_count,
                                        'Client': client_count,
                                        }
                                  )
        df = pd.DataFrame(panda_dict_list, index=list(user_list))
        return df.sort_values(by=['Client', 'Matter', 'Document'], ascending=[0, 0, 0])

    def get_user_data(self, pickle_file, a_uid, start_date, end_date):
        """
        get document, matter and client time series data for a specific user
        :param pickle_file: date file name
        :param a_uid: <user id>
        :param start_date: start interval mm/dd/yyyy
        :param end_date: end interval mm/dd/yyyy
        :return:
        """
        # get the time series data needed for the analysis
        """
                    we are going to get the following information
                    <uid>:[activity_count, activity_count, ........]                time series
                    <uid>:[unique_doc_count, activity_count, ........]              time series
                    <uid>:[unique_matter_count, unique_matter_count, ........]      time series
                    <uid>:[unique_client_count, unique_client_count, ........]      time series
                    <uid>:<epoch day>:<activity count>
                    <uid>:<epoch day>:<unique doc accessed count>
                    <uid>:<epoch day>:<unique matter accessed count>
                    <uid>:<epoch day>:<unique client accessed count>
        """
        data_tuple = self.all_users_series(pickle_file=pickle_file,
                                           start_date=start_date,
                                           end_date=end_date,
                                           verbose=True)

        def local_extract_list(data_dict, the_uid):
            """
            extracts data list from the pass dictionary
            :param data_dict: <uid>:[count1, count2, .....]
            :param the_uid:<uid>
            :return: [count1, .....] if uid found else []
            """
            if the_uid in data_dict:
                return data_dict[the_uid]
            else:
                return []
        document_list = local_extract_list(data_dict=data_tuple[1], the_uid=a_uid)
        matter_list = local_extract_list(data_dict=data_tuple[2], the_uid=a_uid)
        client_list = local_extract_list(data_dict=data_tuple[3], the_uid=a_uid)
        return document_list, matter_list, client_list

    def get_time_struct(self, date_str):
        """
        get time struct from ascii date string
        date is in zero padded month/day/year format year includes century
        :param date_str:
        :return:
        """
        format_str = '%m/%d/%Y'
        return time.strptime(date_str, format_str)

    def extract_docs(self, start_struct, end_struct, id_dict):
        """
        extracts all the documents touched by the user within the date range
        :param start_struct: start interval time struct
        :param end_struct: end interval time struct
        :param id_dict:  <doc_id>:[ [activity_code, epoch time stamp, doc version].........]
        :return: set of documents
        """
        docset = set()
        for doc_id in id_dict.keys():
            activity_list = id_dict[doc_id]
            for entry_info in activity_list:
                status, utc_struct = self.is_epoch_inrange(start_struct=start_struct,
                                                           end_struct=end_struct,
                                                           epoch_time=entry_info[1])
                # if in range
                if status:
                    docset.add(doc_id)
        return docset

    def extract_info(self, start_struct, end_struct, doc_id_dict, entity_type):
        """
        extracts all the doc id or matter id or client id touched by the user within the date range
        :param start_struct:
        :param end_struct:
        :param doc_id_dict:
        :param entity_type: DOC_ID or MATTER_ID or CLIENT_ID
        :return:
        """
        idset = set()
        for doc_id in doc_id_dict.keys():
            activity_list = doc_id_dict[doc_id]
            for entry_info in activity_list:
                status, utc_struct = self.is_epoch_inrange(start_struct=start_struct,
                                                           end_struct=end_struct,
                                                           epoch_time=entry_info[1])
                # if in range
                if status:
                    if entity_type == DOC_ID:
                        idset.add(doc_id)
                    elif entity_type == MATTER_ID:
                        if doc_id in self.doc_to_matter:
                            idset.add(self.doc_to_matter[doc_id])
                    elif entity_type == CLIENT_ID:
                        if doc_id in self.doc_to_matter:
                            matter_id = self.doc_to_matter[doc_id]
                            if matter_id in self.matter_to_client:
                                idset.add(self.matter_to_client[matter_id])

        return idset
    
    def extract_info_dict(self, start_struct, end_struct, doc_id_dict, entity_type=DOC_ID):
        """
        written 6 23 2020
        extracts all the doc id or matter id or client id touched by the user within the date range
        :param start_struct:
        :param end_struct:
        :param doc_id_dict:
        :param entity_type: DOC_ID or MATTER_ID or CLIENT_ID
        :return id_dict: <utc_struc> : [id,...]
        """
        id_dict = {}
        for doc_id in doc_id_dict.keys():
            # <doc_id>:[[activity code, epoch time stamp, doc version]...]
            activity_list = doc_id_dict[doc_id]
            for entry_info in activity_list:
                status, utc_struct = self.is_epoch_inrange(start_struct=start_struct,
                                                           end_struct=end_struct,
                                                           epoch_time=entry_info[1])
                utc_struct_day = utc_struct[0:3]
                # if in range
                if status:
                    if entity_type == DOC_ID:
                        if utc_struct_day not in id_dict:
                            id_dict[utc_struct_day] = [doc_id]
                        else:
                            id_dict[utc_struct_day].append(doc_id)
                    elif entity_type == MATTER_ID:
                        if doc_id in self.doc_to_matter:
                            if utc_struct not in id_dict:
                                id_dict[utc_struct_day] = self.doc_to_matter[doc_id]
                            else: 
                                id_dict[utc_struct_day].append(self.doc_to_matter[doc_id])
                    elif entity_type == CLIENT_ID:
                        if doc_id in self.doc_to_matter:
                            matter_id = self.doc_to_matter[doc_id]
                            if matter_id in self.matter_to_client:
                                if utc_struct_day not in id_dict:
                                    id_dict[utc_struct_day] = [self.matter_to_client[matter_id]]
                                else: 
                                    id_dict[utc_struct_day].append(self.matter_to_client[matter_id])

        return id_dict
    
    def initialize_company_network(self,
                                   pickle_file,
                                   start_date,
                                   end_date,
                                   edge_threshold=1,
                                   network_type=DOC_ID):
        """
        written 6 23 2020
        build a network dictionary of all users by common documents accessed
        :param pickle_file: pickled python dictionary
        :param start_date: start interval
        :param end_date: end interval
        :param edge_threshold: strength threshold for registering a connection
        :param network_type: by document or matter or client
        :return: dictionary <date> : <uid pair> : [item,...]
        """
        
        self.logger.info('Start network build')
        if not self.dict_initialized:
            self.read_pickle_file(pickle_name=pickle_file)
            
        """post pickle file read we have the following dictionary
            <user id>:<doc dict>
            doc_dict
                <doc_id>:[ [activity_code, epoch time stamp, doc version].........]
        """
        
        try:
            start_struct = self.get_time_struct(date_str=start_date)
            end_struct = self.get_time_struct(date_str=end_date)
        except ValueError as e:
            self.logger.error("Date conversion error {}".format(e))
            return
        
        # Build dictionary for user documents
        """We will be constructing the following dictionary
        <uid>:<id_dict>
        id_dict
            <utc_struct>:[id,...]
        """
        
        uid_id_dict = {}
        
        for uid in self.user_dict:
            # change made on 6 22 2020
            # we can now build networks by document client or matter ids
            uid_id_dict[uid] = self.extract_info_dict(start_struct=start_struct,
                                                    end_struct=end_struct,
                                                    doc_id_dict=self.user_dict[uid],
                                                    entity_type=network_type)
        
        self.logger.info('User info dictionaries completed')
        
        
        # build the network
        """We will be constructing the following dictionary
        <user pair>:<interaction dict>
        interaction dict
            <dates>: {items users have in common}
        
        Note: if there is no connection on a particular day,
        set() is entered as the value??
        """
        user_pair_dict = {}
        users_left_list = list(self.user_dict.keys())

        # local function
        def local_build_net_new(current_user):
            current_user_id_dict = uid_id_dict[current_user]
            users_left_list.remove(current_user) 
            if len(users_left_list) >=1:
                
                for user in users_left_list:
                    interaction_dict = {}
                    for date in current_user_id_dict:
                        current_user_id_set = set(current_user_id_dict[date])
                        if date in uid_id_dict[user]:
                            user_id_set = set(uid_id_dict[user][date])
                            interaction_dict[date] = current_user_id_set.intersection(user_id_set)
                    user_pair_dict[current_user, user] = interaction_dict
            
        for c_user in self.user_dict.keys():
            #self.logger.info('User {} connections completed'.format(c_user))
            local_build_net_new(c_user)
            
        return user_pair_dict
    
   