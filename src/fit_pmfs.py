# Updated 5/20/2021
# Constructs posterior pmfs for all user metrics

# Combination of work by Rakesh Kumar, Rafiq Mohammadi, and Dani Tucker


import sys
import random
import glob
import os
import numpy as np
import datetime 
import time 
import math
from scipy.stats import poisson
from scipy.stats import mannwhitneyu
import pickle
import pymc3 as pm
import calendar
from theano import tensor as tt
from functools import partial
import multiprocessing
import multiprocessing.pool
import logging
import statistics as stat 
logger = logging.getLogger('pymc3')
logger.setLevel(logging.ERROR)

# Get script dependencies
import main_module

SEED = 12345
ITEMS = ["doc", "matter", "client"]
START_INDEX = 5
CORES = 2

def load_data(dat_file, txt_file):
    """
    written 5 20 2021
    load data for graphs
    :param dat_file: company data
    :param txt_file: dates for training
    :return: user object, degree_dicts, start_date, end_date and detect date
    """
    degree_dicts = pickle.load(open("/new_data/degree_dicts.pkl", 'rb'))
    dates = [line.rstrip('\n') for line in txt_file]
    start_date = dates[0]
    end_date = dates[1]
    detect_date = dates[2]
    user_object = risk_analyis_new1.RiskAnalysis()
    return(user_object, degree_dicts, start_date, end_date, detect_date)

def stdev2(count_list):
    """
    created 7 10 2020
    standard deviation function when all counts are the same
    :param list of counts
    :return: standard deviation of list
    """
    if len(count_list)<2:
        sd=1
    else:
        sd=stat.stdev(count_list)
    return sd

def initialize(user_object, dat_file, start_date, end_date):   
    """
    read data file and construct training data
    param: dat file: file with all company data
    param: start_date
    param: end_date
    return: return tseries_info: list with all count dictionaries
    """
    user_object.read_pickle_file(pickle_name=dat_file)
    tseries_info = user_object.get_training_data(pickle_file=dat_file,
                                            training_start=start_date,
                                            training_end=end_date)
    return(tseries_info)

def collect_count_dicts(tseries_info, user_object, start_date, end_date):
    # These two functions fill in zeros 
    # Where there are no observed counts                                              
    def daterange(date1, date2):
        start_epoch_day = math.floor(time.mktime(user_object._get_time_struct(date1))/60/24/60)
        end_epoch_day = math.ceil(time.mktime(user_object._get_time_struct(date2))/60/24/60)
        for n in range(end_epoch_day - start_epoch_day):
            yield start_epoch_day + n

    def insert_missing(count_dict, start_date, end_date):
        all_dates = [dt for dt in daterange(start_date, end_date)]
        diff = list(set(all_dates) - set(count_dict))
        #print(diff)
        for i in range(len(diff)):
            count_dict[diff[i]] = 0
        return(count_dict)
        
    count_dicts = {}
    it=0
    for item in range(0, len(ITEMS), 1):
        temp1 = {}
        for uid in tseries_info[(START_INDEX + it)]:
            temp2 = dict(sorted(tseries_info[5][uid].items()))
            temp1[uid] = dict(sorted(insert_missing(temp2, start_date, end_date).items()))
        string = ITEMS[(item-1)] + "_count"
        count_dicts[string] = temp1
        it += 1
    return(count_dicts)

def collect_degree_dicts(degree_dicts):
    # This turns the degree dictionary from
    # <date:uid:degree> to <uid:date:degree> 
    degree_dicts2 = {}
    for item in range(0, len(ITEMS), 1):
        string = ITEMS[(item-1)] + "_degree"
        degree_dicts2[string] = {} #1
        for date in degree_dicts[string]:
            for uid in degree_dicts[string][date]:
                degree_dicts2[string][uid] = {}
        for date in degree_dicts[string]:
            for uid in degree_dicts[string][date]:
                degree_dicts2[string][uid][date] = degree_dicts[string][date][uid]
    return(degree_dicts2)
    
# This is the function for computing the posterior distribution
# For the Poisson Mixture

def build_posterior_pmf_counts(user_tseries_dict, user_object, detect_date, uid):
    """
    updated 8 10 2020
    Bayesian mixed poisson module
    :param user_tseries_dict: <uid>:[count, count .... count]
    :param detect_date: maximum date of training data
    :param uid: user id
    :return: [uid, [probabilities, quantiles, goodness of fit p value], [zscore, stdev, length]] 
    """
    DATA_THRESHOLD = 30 # enough data to fit a distribution
    SD_MAX = 1000 # low enough standard deviation of counts to require less memory usuage
    K=20 # maximum number of components in Poisson mixture

    # Internal function for sampling from entire count_dict
    # The farther back you go, the fewer observations are included
    def count_sample(count_dict, detect_date):
        newest_date = math.floor(time.mktime(user_object._get_time_struct(detect_date))/60/24/60)
        oldest_date = min(count_dict.keys())
        diff = newest_date - oldest_date
        # Includes cold start which is dealth with later
        if diff < 90:
            return {k:v for k,v in count_dict.items() if k < newest_date}
        # Sample at 75% at this level and add to newer data
        elif 90 <= diff < 180:
            new_data = {k:v for k,v in count_dict.items() if newest_date >k > newest_date - 90}
            old_data = {k:v for k,v in count_dict.items() if k <= newest_date - 90}
            temp = len(old_data)
            n = math.ceil(.75*temp)
            sampled_data = {dt:v for dt,v in old_data.items() if dt in random.choices(list(old_data.keys()), k=n)}
            return {**new_data, **sampled_data}
        # Sample additional 50% at this level
        elif 180 <= diff < 270:
            new_data = {k:v for k,v in count_dict.items() if newest_date >k > newest_date - 90}
            old_data = {k:v for k,v in count_dict.items() if (k >= newest_date - 180 and k < newest_date - 90 )}
            temp = len(old_data)
            n = math.ceil(.75*temp)
            sampled_data1 = {dt:v for dt,v in old_data.items() if dt in random.choices(list(old_data.keys()), k=n)}

            old_data = {k:v for k,v in count_dict.items() if (k >= newest_date - 270 and k < newest_date - 180 )}
            temp = len(old_data)
            n = math.ceil(.50*temp)
            sampled_data2 = {dt:v for dt,v in old_data.items() if dt in random.choices(list(old_data.keys()), k=n)}
            return {**new_data, **sampled_data1, **sampled_data2}
        # Sample additional 25% at this level
        elif 270 <= diff < 365:
            new_data = {k:v for k,v in count_dict.items() if newest_date >k > newest_date - 90}
            old_data = {k:v for k,v in count_dict.items() if(k >= newest_date - 180 and k < newest_date - 90 )}
            temp = len(old_data)
            n = math.ceil(.75*temp)
            sample = random.choices(list(old_data.keys()), k=n)
            sampled_data1 = {dt:v for dt,v in old_data.items() if dt in sample}

            old_data = {k:v for k,v in count_dict.items() if (k >= newest_date - 270 and k < newest_date - 180 )}
            temp = len(old_data)
            n = math.ceil(.50*temp)
            sampled_data2 = {dt:v for dt,v in old_data.items() if dt in random.choices(list(old_data.keys()), k=n)}

            old_data = {k:v for k,v in count_dict.items() if (k >= newest_date - 365 and k < newest_date - 270 )}
            temp = len(old_data)
            n = math.ceil(.25*temp)
            sample = random.choices(list(old_data.keys()), k=n)
            sampled_data3 = {dt:v for dt,v in old_data.items() if dt in sample}
            return {**new_data, **sampled_data1, **sampled_data2, **sampled_data3}
        # Sample additional 10% at this level
        elif diff >= 365:
            new_data = {k:v for k,v in count_dict.items() if newest_date >k > newest_date - 90}
            old_data = {k:v for k,v in count_dict.items() if(k >= newest_date - 180 and k < newest_date - 90 )}
            temp = len(old_data)
            n = math.ceil(.75*temp)
            sample = random.choices(list(old_data.keys()), k=n)
            sampled_data1 = {dt:v for dt,v in old_data.items() if dt in sample}

            old_data = {k:v for k,v in count_dict.items() if (k >= newest_date - 270 and k < newest_date - 180 )}
            temp = len(old_data)
            n = math.ceil(.50*temp)
            sampled_data2 = {dt:v for dt,v in old_data.items() if dt in random.choices(list(old_data.keys()), k=n)}

            old_data = {k:v for k,v in count_dict.items() if (k >= newest_date - 365 and k < newest_date - 270 )}
            temp = len(old_data)
            n = math.ceil(.25*temp)
            sample = random.choices(list(old_data.keys()), k=n)
            sampled_data3 = {dt:v for dt,v in old_data.items() if dt in sample}

            old_data = {k:v for k,v in count_dict.items() if (k < newest_date - 365 )}
            temp = len(old_data)
            n = math.ceil(.10*temp)
            sample = random.choices(list(old_data.keys()), k=n)
            sampled_data4 = {dt:v for dt,v in old_data.items() if dt in sample}
            return {**new_data, **sampled_data1, **sampled_data2, **sampled_data3, **sampled_data4}

    # Internal Deterministic function for weights
    # It can be shown that the resulting weights follow
    # roughly a dirichlet dist with vector length equal to K with identical elements
    def stick_breaking(beta):
        """
        :param beta: observation from Beta(1, alpha) where alpha a rv from Gamma(1,1)
        :return: weights
        """
        portion_remaining = tt.concatenate([[1], tt.extra_ops.cumprod(1 - beta)[:-1]])
        return beta * portion_remaining

    # Internal function for computing pmfs from gibbs sampler
    def compute_pmfs(trace, x_plot):
        """
        :param trace: object returned by pymc mixed poisson gibbs sampler
        :param x_plot: range of counts observed for user
        :return: [probabilities, quantiles, Mann-Whitney pvalue for goodness of fit]
        """  
        K=20
        post_pmf_contribs = poisson.pmf(np.atleast_3d(x_plot),
                                     trace['mu'][:, np.newaxis, :])
        post_pmfs_rough = (trace['w'][:, np.newaxis, :] * post_pmf_contribs).sum(axis=-1)
        post_pmfs_mean = post_pmfs_rough.mean(axis=0)
        post_pmfs = post_pmfs_mean/sum(post_pmfs_mean)
        # Even if gibbs sampler does not converge, mean of posterior pmfs is take here
        # If inference is to be made, will want to keep the trace['w'] and trace['mu'] information
        return([post_pmfs,x_plot])

    # Internal function for computing goodness of fit pvalue
    def MW_test(post_pmf, x_plot, counts):
        prob = post_pmf/sum(post_pmf)
        expect_counts=np.random.choice(x_plot, len(counts), p = prob)
        # If no variation in counts, reject fit...need to fix this analysis for future
        if len(set(expect_counts)) == 1:
            mw_p_value = 0
        else: 
            mw_p_value = mannwhitneyu(counts,expect_counts).pvalue
        return mw_p_value

    # Counts sampled and throw out 0's
    count_list = [k for k in list(count_sample(user_tseries_dict[uid], detect_date).values()) if k>0]

    # If not enough data for a distribution fit, return enough info to compare new counts by z score
    if len(count_list) < DATA_THRESHOLD:
        print(uid, "does not have enough data to fit a distribution. Collecting mean, stdev, and length.")
        if len(count_list) < 0:
            uid_exception_list = [0,0,0]
            posterior_pmf_list = []
        else:
            uid_exception_list = [np.mean(count_list), stdev2(count_list), len(count_list)]
            posterior_pmf_list = []
    elif stdev2(count_list) > SD_MAX:
        print(uid, "varies too much to fit a distribution. Collecting mean, stdev, and length")
        uid_exception_list = [np.mean(count_list), stdev2(count_list), len(count_list)]
        posterior_pmf_list = []
    else:
        uid_exception_dict = []
        # Here, the + 25 can be any number > 1. It just helps PYMC consider 
        # How the distribution behaves after the maximum observed count
        x_plot = np.arange(max(count_list)+25)

        # set up Gibbs sampler
        with pm.Model() as model:
            alpha = pm.Gamma('alpha', 1., 1.)
            beta= pm.Beta('beta', 1, alpha, shape=K)
            # weights for each component
            w = pm.Deterministic('w', stick_breaking(beta))
            # means for each component
            mu = pm.Uniform('mu', 0., max(count_list), shape=K)
            # count data with allocations for each component
            obs = pm.Mixture('obs', w, pm.Poisson.dist(mu), observed=count_list)

        # run Gibbs sampler
        with model:
            step = pm.Metropolis()
            trace = pm.sample(1000, step = step, random_seed=SEED)

        # save posterior pmf
        posterior_pmf_list = compute_pmfs(trace, x_plot)
        # Pull the p-value from the posterior info
        p_value = MW_test(posterior_pmf_list[0], posterior_pmf_list[1], count_list)
        posterior_pmf_list.append(p_value)
        uid_exception_list = [np.mean(count_list), stdev2(count_list), len(count_list)]
        #print(p_value, uid_exception_list)
    return uid, posterior_pmf_list, uid_exception_list

# PYMC uses parallel processing already
# If we want to add another layer, we have to 
# ensure that it is a "no daemon process"

class NoDaemonProcess(multiprocessing.Process):
    @property
    def daemon(self):
        return False

    @daemon.setter
    def daemon(self, value):
        pass

class NoDaemonContext(type(multiprocessing.get_context())):
    Process = NoDaemonProcess

class MyPool(multiprocessing.pool.Pool):
    def __init__(self, *args, **kwargs):
        kwargs['context'] = NoDaemonContext()
        super(MyPool, self).__init__(*args, **kwargs)

def work(num_procs, pmf_maker, uid_list):
    print("Creating %i (non-daemon) workers and jobs in child." % num_procs)
    pool = MyPool(num_procs)
    out3 = pool.map(pmf_maker, uid_list)
    pool.close()
    pool.join()
    return out3

def save_file(fit):
    f = open(str("/new_data/all_fits.pkl"),"wb")
    pickle.dump(fit,f)
    f.close()


if __name__ == '__main__':
    dat_file1 = "/new_data/custa_cut_data.dat" 
    txt_file1 = open("/new_data/dates.txt","r")
    user_object1, degree_dicts1, start_date1, end_date1, detect_date1 = load_data(dat_file1, txt_file1)
    count_tseries1 = initialize(user_object1, dat_file1, start_date1, end_date1)
    count_dicts1 = collect_count_dicts(count_tseries1, user_object1, start_date1, end_date1)
    degree_dicts2 = collect_degree_dicts(degree_dicts1)
    dicts1 = {**count_dicts1, **degree_dicts2}
    dict_names = list(dicts1.keys())
    fits = {}
    uid_list = dicts1[dict_names[1]].keys()
    for item in dict_names:
        temp = dicts1[item].keys()
        uid_list = temp & uid_list
        uid_list = list(uid_list)
    for item in dict_names:
        dictionary = dicts1[item]
        out3 = {}
        func = partial(build_posterior_pmf_counts, dictionary, user_object1, detect_date1)
        out1 = work(CORES, func, uid_list)
        string1 = str(item)
        fits[string] = out1
    save_file(fits)
