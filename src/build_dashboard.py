# Updated 5 20 2021

# Outputs anomaly detection dashboard

import datetime 
import glob
import statistics as stat
import pickle
import pandas as pd
ITEMS = ["doc", "matter", "client"]

def anomaly_analysis_counts (item, count_dict, posterior_dict, risk_dict):
    """
    updated 8 10 2020
    does a risk analysis
    on new data compared to posterior pmf
    Can be applied to any count data
    :param count_dict: <uid>:<epoch day>:<count>
    :param posterior_dict: <uid>:[[posterior probs, posterior quantiles, p-value],[mean, sd, sample size]]
    :param risk_dict:  <uid>:<epoch day>:[doc_count,matter_count, client_count, doc_deg, matter_deg, client_deg]
    :return: 
    """
    for uid in count_dict:
        if uid not in posterior_dict:
            continue
        else:
            day_dict = count_dict[uid]
            if posterior_dict[uid][0] == []:
                for epoch_day in day_dict:
                    if uid not in risk_dict[item]:
                        risk_dict[item][uid] = {epoch_day: 0}
                    else:
                        score_dict = risk_dict[item][uid]
                        score_dict[epoch_day] = 0
            else:
                post_pmf = posterior_dict[uid][0][0]
                x_d = posterior_dict[uid][0][1]
                max_count = max(x_d)
                for epoch_day in day_dict:
                    count = day_dict[epoch_day]
                    if count > max_count:
                        posterior_p = 1
                    elif count == 0:
                        posterior_p = 0
                    elif 0 < count <= max_count:
                        index = next(x for x, val in enumerate(x_d) if val >= count)
                        posterior_p = sum(post_pmf[:index])
                    if uid not in risk_dict[item]:
                        risk_dict[item][uid] = {epoch_day: posterior_p}
                    else:
                        score_dict = risk_dict[item][uid]
                        score_dict[epoch_day] = posterior_p

def build_dashboard_only(pickle_file,
                         degree_dict2,
                         posterior_dicts,
                         detect_start,
                         detect_end):
    """
    updated 8 10 2020
    assemble a data frame that lists users sorted by their risk for 
    having anomalies
    :param pickle_file: data file name
    :param degree_dict2: <item>:<uid>:<date>:degree
    :param posterior_dicts: <item>:<uid>:[[probabilities, quantiles, MWU test p-value],[mean, sd, sample size]]
    :param detect_start: month/day/yyyy
    :param detect_end: month/day/yyyy
    :return: risk dataframe only
    """
    compound_key = detect_start + detect_end
    doc_posterior_dict = posterior_dicts["doc_count"]
    matter_posterior_dict = posterior_dicts["matter_count"]
    client_posterior_dict = posterior_dicts["client_count"]

    doc_degree_posterior_dict = posterior_dicts["doc_deg"]
    matter_degree_posterior_dict = posterior_dicts["matter_deg"]
    client_degree_posterior_dict = posterior_dicts["client_deg"]

    # count data for detection
    ret_test_tuple = user_object.all_users_series(pickle_file=pickle_file,
                                               start_date=detect_start,
                                               end_date=detect_end,
                                               verbose=True)

    # Internal way to extract degree data for detection
    def extract_degree(degree_dict, start_date, end_date):
        utc_start = time.strptime(start_date, "%m/%d/%Y")
        day_start = calendar.timegm(utc_start)/60/60/24
        utc_end = time.strptime(end_date, "%m/%d/%Y")
        day_end = calendar.timegm(utc_end)/60/60/24

        out_dict = {}
        for uid in degree_dict.keys():
            temp = degree_dict[uid]
            out_dict[uid] = {k:v for k,v in temp.items() if day_start <= k <= day_end}
        return out_dict     

    doc_detect_dict = {k:insert_missing(ret_test_tuple[5][k], detect_start, detect_end) for k in ret_test_tuple[5].keys()}
    matter_detect_dict = {k:insert_missing(ret_test_tuple[6][k], detect_start, detect_end) for k in ret_test_tuple[6].keys()}
    client_detect_dict = {k:insert_missing(ret_test_tuple[7][k], detect_start, detect_end) for k in ret_test_tuple[7].keys()}

    # degree data for detection
    doc_degree_detect_dict = {k:v for k,v in extract_degree(degree_dict2["doc_deg"], detect_start, detect_end).items() if k in list(doc_detect_dict.keys())}
    matter_degree_detect_dict = {k:v for k,v in extract_degree(degree_dict2["matter_deg"], detect_start, detect_end).items() if k in list(doc_detect_dict.keys())}
    client_degree_detect_dict = {k:v for k,v in extract_degree(degree_dict2["client_deg"], detect_start, detect_end).items() if k in list(doc_detect_dict.keys())}
    
    # Compute Risk for counts
    risk_dict = {}
    risk_dict["doc_count"] = {}
    risk_dict["matter_count"] = {}
    risk_dict["client_count"] = {}
    risk_dict["doc_deg"] = {}
    risk_dict["matter_deg"] = {}
    risk_dict["client_deg"] = {}
    
    # Compute Risk for counts
    anomaly_analysis_counts("doc_count",count_dict=doc_detect_dict,
                          posterior_dict = doc_posterior_dict, 
                              risk_dict=risk_dict)

    anomaly_analysis_counts("matter_count",count_dict=matter_detect_dict,
                          posterior_dict = matter_posterior_dict, 
                              risk_dict=risk_dict)

    anomaly_analysis_counts("client_count",count_dict=client_detect_dict,
                          posterior_dict = client_posterior_dict, 
                              risk_dict=risk_dict)

    # Compute Risk for degree
    anomaly_analysis_counts("doc_deg",count_dict=doc_degree_detect_dict,
                          posterior_dict = doc_degree_posterior_dict, 
                              risk_dict=risk_dict)

    anomaly_analysis_counts("matter_deg",count_dict=matter_degree_detect_dict,
                          posterior_dict = matter_degree_posterior_dict, 
                              risk_dict=risk_dict)

    anomaly_analysis_counts("client_deg",count_dict=client_degree_detect_dict,
                          posterior_dict = client_degree_posterior_dict, 
                              risk_dict=risk_dict)

    pd_dict={}
    # assemble risk score card
    for item in risk_dict.keys():
        pd_dict[item] = []
        for uid in risk_dict[item]:
            day_dict = risk_dict[item][uid]
            for epoch_day in day_dict:
                score = day_dict[epoch_day]
                epochtime = int(epoch_day + 1) * (24 * 3600)
                date_str = datetime.datetime.fromtimestamp(epochtime).strftime('%m/%d/%Y')
                
                obs = {}
                obs["doc_deg"] = doc_degree_detect_dict[uid][epoch_day]
                obs["matter_deg"] = matter_degree_detect_dict[uid][epoch_day]
                obs["client_deg"] = client_degree_detect_dict[uid][epoch_day]
                
                if epoch_day not in doc_detect_dict[uid]:
                    obs["doc_count"] = 0
                    obs["matter_count"] = 0
                    obs["client_count"] = 0
                else: 
                    obs["doc_count"] = doc_detect_dict[uid][epoch_day]
                    obs["matter_count"] = matter_detect_dict[uid][epoch_day]
                    obs["client_count"] = client_detect_dict[uid][epoch_day]
                
                if posterior_dicts[item][uid][0] == []:
                    confidence = 0
                else: 
                    confidence = posterior_dicts[item][uid][0][2]
                mean = posterior_dicts[item][uid][1][0]
                sd = max(1, posterior_dicts[item][uid][1][1])
                n = posterior_dicts[item][uid][1][2]
                z = {}
                if obs[item]==0:
                    z[item] = 0
                else:
                    # Mean with new observation
                    temp = (mean*n + obs[item])/(n+1)
                    # Z score is how the new mean deviates from the old mean
                    z[item] = (temp - mean)/sd*(n+1)
                
                item_dict = {
                    'User': uid,
                    'Date': date_str,
                    item + ' Observed': obs[item],
                    item + ' Percentile': score,
                    item + ' Confidence': confidence,
                    item + ' Z Score':z[item],
                }
                pd_dict[item].append(item_dict)
    temp1 = pd.DataFrame.from_dict(pd_dict['doc_count'])
    temp2 = pd.DataFrame.from_dict(pd_dict['matter_count'])
    temp3 = pd.DataFrame.from_dict(pd_dict['client_count'])
    temp4 = pd.DataFrame.from_dict(pd_dict['doc_deg'])
    temp5 = pd.DataFrame.from_dict(pd_dict['matter_deg'])
    temp6 = pd.DataFrame.from_dict(pd_dict['client_deg'])
    
    temp7 = temp3.merge(temp6, on= ["User", "Date"])
    temp8 = temp2.merge(temp5, on= ["User", "Date"])
    temp9 = temp1.merge(temp4, on= ["User", "Date"])
    temp10 = temp7.merge(temp8, on= ["User", "Date"])
    temp11 = temp10.merge(temp9, on= ["User", "Date"])
    
    # Compute risk of anomaly aka "Overall Score": a sum of percentiles*zscore. The higher, the riskier.
    temp12 = temp11.assign(Overall_Score = temp11['client_count Percentile']*temp11['client_count Z Score'] + 
         temp11['client_deg Percentile']*temp11['client_deg Z Score'] +
         temp11['matter_count Percentile']*temp11['matter_count Z Score'] +
         temp11['matter_deg Percentile']*temp11['matter_deg Z Score'] +
         temp11['doc_count Percentile']*temp11['doc_count Z Score'] +
         temp11['doc_deg Percentile']*temp11['doc_deg Z Score'])
    
    # Compute overall "Confidence": a sum of all p-values. The higher, the more confidence.
    temp13 = temp12.assign(Confidence = temp11['client_count Confidence'] + temp11['client_deg Confidence'] +
                 temp11['matter_count Confidence'] + temp11['matter_deg Confidence'] +
                 temp11['doc_count Confidence'] + temp11['doc_deg Confidence'])

    sort_cols = ['Date',
                'Overall_Score',
                 'Confidence'
                ]
    df = temp13.sort_values(by=sort_cols, ascending=[1,0,0])
    # Only return score on days when docs were opened. This can be changed if desired!
    df2 = df[df['doc_count Observed']>0]
    # Only return score if positive overall score. This can also be changed, but seems to make some sense. 
    return df2[df2['Overall_Score']>0]


degree_dict = pickle.load(open("/new_data/degree_dicts.pkl", 'rb'))
posterior_dicts = open(str("/new_data/all_fits.pkl"),"wb")

dat_file1 = "/new_data/custa_cut_data.dat" 
txt_file1 = open("/new_data/dates.txt","r")
dates = [line.rstrip('\n') for line in txt_file1]
detect_start = dates[2]
detect_end = dates[3]

df = build_dashboard_only(dat_file1, degree_dict, posterior_dicts, detect_start, detect_end)

df.to_csv("/new_data/dashboard.csv")

