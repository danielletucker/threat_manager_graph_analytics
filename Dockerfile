FROM python:3.8
WORKDIR /src
COPY src /src 
COPY requirements.txt /requirements.txt 
RUN pip install -r /requirements.txt
RUN chmod +x run_build_graphs.sh
RUN chmod +x run_fit_pmfs.sh
RUN chmod +x run_build_dashboard.sh
